package edu.ukma.fin.ir.atsaruk.hs.dao;

import edu.ukma.fin.ir.atsaruk.hs.entity.Client;
import org.springframework.data.repository.CrudRepository;

public interface ClientRepository extends CrudRepository<Client, Long> {

}
