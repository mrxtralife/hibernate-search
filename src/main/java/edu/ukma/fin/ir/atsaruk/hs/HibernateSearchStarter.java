package edu.ukma.fin.ir.atsaruk.hs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class HibernateSearchStarter extends SpringBootServletInitializer {

  @Override
  protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
    return application.sources(HibernateSearchStarter.class);
  }

  public static void main(String[] args) {
    SpringApplication.run(HibernateSearchStarter.class, args);
  }
}
