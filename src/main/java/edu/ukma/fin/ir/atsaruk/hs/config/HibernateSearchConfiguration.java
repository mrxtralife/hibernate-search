package edu.ukma.fin.ir.atsaruk.hs.config;

import edu.ukma.fin.ir.atsaruk.hs.service.HibernateSearchService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.persistence.EntityManager;

@Configuration
public class HibernateSearchConfiguration {

  @Bean
  public HibernateSearchService hibernateSearchService(final EntityManager entityManager) {
    HibernateSearchService hibernateSearchService = new HibernateSearchService(entityManager);
    hibernateSearchService.initializeHibernateSearch();
    return hibernateSearchService;
  }
}