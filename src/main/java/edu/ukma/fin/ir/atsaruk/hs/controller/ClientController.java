package edu.ukma.fin.ir.atsaruk.hs.controller;

import edu.ukma.fin.ir.atsaruk.hs.entity.Client;
import edu.ukma.fin.ir.atsaruk.hs.service.ClientService;
import edu.ukma.fin.ir.atsaruk.hs.service.HibernateSearchService;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ClientController {

  private final ClientService clientService;
  private final HibernateSearchService searchService;

  public ClientController(final ClientService clientService,
      final HibernateSearchService searchService) {
    this.clientService = clientService;
    this.searchService = searchService;
  }

  @GetMapping(value = "/")
  public String mainPage() {
    return "index";
  }

  @GetMapping(value = "/", params = "search")
  public String search(@RequestParam(value = "search") String query, Model model) {
    if (query == null || query.isEmpty()) {
      return "index";
    }

    List<Client> searchResults = searchService.clientSearch(query);
    model.addAttribute("search", searchResults);
    return "index";
  }

  @PostMapping(value = "/")
  public String addClient(Client client) {
    clientService.addClient(client);

    return "index";
  }
}
