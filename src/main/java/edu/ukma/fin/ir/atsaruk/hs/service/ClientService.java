package edu.ukma.fin.ir.atsaruk.hs.service;

import edu.ukma.fin.ir.atsaruk.hs.dao.ClientRepository;
import edu.ukma.fin.ir.atsaruk.hs.entity.Client;
import org.springframework.stereotype.Service;

@Service
public class ClientService {

  private final ClientRepository repository;

  public ClientService(ClientRepository repository) {
    this.repository = repository;
  }

  public void addClient(final Client client) {
    repository.save(client);
  }
}
