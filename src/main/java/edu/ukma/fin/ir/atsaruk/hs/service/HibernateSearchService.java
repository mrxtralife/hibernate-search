package edu.ukma.fin.ir.atsaruk.hs.service;

import edu.ukma.fin.ir.atsaruk.hs.entity.Client;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import org.apache.lucene.search.Query;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class HibernateSearchService {

  private final EntityManager entityManager;

  public HibernateSearchService(final EntityManager entityManager) {
    this.entityManager = entityManager;
  }

  public void initializeHibernateSearch() {

    try {
      FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);
      fullTextEntityManager.createIndexer().startAndWait();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  @Transactional
  public List<Client> clientSearch(String keyword) {

    FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);
    QueryBuilder qb = fullTextEntityManager.getSearchFactory().buildQueryBuilder()
        .forEntity(Client.class).get();
    Query luceneQuery = qb.keyword().fuzzy().withEditDistanceUpTo(1).withPrefixLength(1)
        .onFields("name", "address", "contact")
        .matching(keyword).createQuery();

    javax.persistence.Query jpaQuery = fullTextEntityManager
        .createFullTextQuery(luceneQuery, Client.class);

    List<Client> clients = null;
    try {
      clients = jpaQuery.getResultList();
    } catch (NoResultException nre) {
      // Do nothing
    }

    return clients;
  }
}
